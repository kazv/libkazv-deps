#!/bin/sh

sed -e 's@RUN_WITH_CCACHE@RUN --mount=type=cache,id=ccache,target=/ccache export CCACHE_COMPILERCHECK=content CCACHE_BASEDIR=/build CCACHE_DIR=/ccache PATH="/usr/lib/ccache:$PATH" CC=/usr/lib/ccache/gcc CXX=/usr/lib/ccache/g++ \&\&@' "$1"
